package ru.malykhin.tm.repository;

import ru.malykhin.tm.controller.TaskController;
import ru.malykhin.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public void load(final Task task) {
        tasks.add(task);
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() -1) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Task task: tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task: tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public void loadTask() {

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Tasks.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();
            tasks = ((Tasks) un.unmarshal(new File("BD.xml"))).getList();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @XmlRootElement
    private static class Tasks {

        private List<Task> list;

        @XmlElement(name = "Task")
        public List<Task> getList() {
            return list;
        }

        public void setList(List<Task> list) {
            this.list = list;
        }

    }

    public int saveTask() {
        try {
            Tasks tsk = new Tasks();
            tsk.setList(tasks);
            JAXBContext context = JAXBContext.newInstance(Tasks.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            marshaller.marshal(tsk, new File("BD.xml"));

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

}
