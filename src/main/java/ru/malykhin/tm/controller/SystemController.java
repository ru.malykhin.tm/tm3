package ru.malykhin.tm.controller;

public class SystemController {

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-save - Save list of projects.");
        System.out.println("project-load - Load list of projects.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-save - Save list of tasks.");
        System.out.println("task-load - Load list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public int displayAbout() {
        System.out.println("meinroman");
        System.out.println("meinroman@yandex.ru");
        return 0;
    }

    public int loadAll(ProjectController projectController, TaskController taskController) {
        taskController.loadTask();
        projectController.loadProject();
        return 0;
    }

    public int saveAll(ProjectController projectController, TaskController taskController) {
        taskController.saveTask();
        projectController.saveProject();
        return 0;
    }




}
