package ru.malykhin.tm.endpoint;

import ru.malykhin.tm.entity.Project;
import ru.malykhin.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private ProjectService projectService;

    public ProjectEndpoint() {

    }

    public ProjectEndpoint(ProjectService projectService) {
        this.projectService = projectService;
    }

    @WebMethod
    public List<Project> findAll() {
        return projectService.findAll();
    }
    @WebMethod
    public Project createProject(String name) {
        return projectService.create(name);
    }
    @WebMethod

    public Project update(Long id, String name, String description) {
        return projectService.update(id, name, description);
    }
    @WebMethod
    public Project findByIndex(int index) {
        return projectService.findByIndex(index);
    }
    @WebMethod
    public Project removeByIndex(int index) {
        return projectService.removeByIndex(index);
    }
}
