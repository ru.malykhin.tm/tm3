package ru.malykhin.tm.endpoint;

import ru.malykhin.tm.entity.Task;
import ru.malykhin.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint {

    private TaskService taskService;

    public TaskEndpoint() {

    }

    public TaskEndpoint(TaskService taskService) {
        this.taskService = taskService;
    }

    @WebMethod
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @WebMethod
    public Task createTask(String name) {
        return taskService.create(name);
    }

    @WebMethod
    public Task findByIndex(int index) {
        return taskService.findByIndex(index);
    }

    @WebMethod
    public void saveAll() {
        taskService.saveAll();
    }

    @WebMethod
    public void loadAll() {
        taskService.loadAll();
    }
}
